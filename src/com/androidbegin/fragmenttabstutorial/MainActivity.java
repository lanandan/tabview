package com.androidbegin.fragmenttabstutorial;

import android.app.ActionBar;

import android.app.ActionBar.Tab;
import android.app.ActionBar.TabListener;
import android.app.FragmentTransaction;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.app.Activity;

public class MainActivity extends ActionBarActivity implements TabListener {
	// Declare Tab Variable
	ActionBar actionBar;
	ViewPager vi;
	ActionBar.Tab fruits, birds, world;
	/*Fragment fragmentfruit = new FragmentTab1();
	Fragment fragmentbirds = new FragmentTab2();
	Fragment fragmentworld = new FragmentTab3();
*/
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	 vi=(ViewPager)findViewById(R.id.pager);
		TabAdapter vil=new TabAdapter(getSupportFragmentManager());
		
		actionBar = getActionBar();

		// Hide Actionbar Icon
		actionBar.setDisplayShowHomeEnabled(false);

		// Hide Actionbar Title
		actionBar.setDisplayShowTitleEnabled(false);
		
		// Create Actionbar Tabs
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
	
		// Set Tab Icon and Titles
		fruits = actionBar.newTab().setIcon(R.drawable.fruits).setTabListener(this);
		birds = actionBar.newTab().setIcon(R.drawable.birds).setTabListener(this);
		world = actionBar.newTab().setIcon(R.drawable.world).setTabListener(this);

		// Set Tab Listeners
		/*fruits.setTabListener(new TabListener(fragmentfruit));
		birds.setTabListener(new TabListener(fragmentbirds));
		world.setTabListener(new TabListener(fragmentworld));*/

		// Add tabs to actionbar
		actionBar.addTab(fruits);
		actionBar.addTab(birds);
		actionBar.addTab(world);
		vi.setAdapter(vil);
	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub
		vi.setCurrentItem(tab.getPosition());
		}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub
		
	}
}
